﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
	public enum CardSuit
	{
		DIAMONDS = 1,
		CLUBS,
		HEARTS,
		SPADES
	}

	/// <summary>
	/// Represents a card game playing card
	/// </summary>
	public class Card
	{
		/// <summary>
		/// The value of the card, 1 -> 13
		/// </summary>
		private byte _value;

		/// <summary>
		/// The suit of the card
		/// </summary>
		private CardSuit _suit;

		public Card(byte value, CardSuit suit)
		{
			_value = value;
			_suit = suit;
		}

		/// <summary>
		/// Accessor and mutator functionality for _value
		/// </summary>
		public byte Value
		{
			get { return _value; }
			set { _value = value; }
		}

		//declare a read/write property _suit
		public CardSuit Suit
		{
			get { return _suit; }
			set { _suit = value; }
		}
		
		/// <summary>
		/// Read-only property that returns the name of the card. The name
		/// of the card is its value for cards 2 - 10 and Jack, Queen, King
		/// and Ace for the face cards.
		/// Example of a "calculated property" that is read-only
		/// </summary>
		public string CardName
		{ 
			get { return "TODO"; }
		}

		/// <summary>
		/// Another example of a calculated read-only property
		/// </summary>
		public string SuitName
		{
			get { return _suit.ToString(); }
		}
	}
}
